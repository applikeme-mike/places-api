<?php
include 'config.php';

// debug to user browser
// to use -> debug_to_console( $some_var );

class Business{
  public $name = null;
  public $website = null;
}

function debug_to_console( $data ) {

  if ( is_array( $data ) )
    $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
  else
    $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

  echo $output;
}

// Do the search - to use do
// doSearch($mc,$googlePlaces,$q,$key);

function doSearch($mc,$googlePlaces,$q,$key){


  if ($mc->get($key)) {

      debug_to_console( 'query found in cache at '. date('H:i:s') );
      return $mc->get($key);

  } else {
      debug_to_console( 'query not in cache' );
      $arr = array();
      $data = $googlePlaces->textSearch($q);

      $businesses = json_decode($data,true);
      foreach ($businesses['results'] as $business){
      //  echo $business['place_id']."<br>";
        $details = $googlePlaces->placeDetails($business['place_id']);

        $obj = (object) [
          'name' => $details['result']['name'],
          'website' => $details['result']['website']

        ];

        array_push($arr,$obj);

      }

      //echo $arr[0]->name;
      //json_encode($arr);
     $mc->set($key, $arr, time() + 1500);
     debug_to_console( 'query now in cache for 15 minutes at '. date('H:i:s'));
     return  $arr;
    }
}
