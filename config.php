<?php

//vendor
require __DIR__ . '/vendor/autoload.php';
use SKAgarwal\GoogleApi\PlacesApi;

// google places
$googlePlaces = new PlacesApi('AIzaSyDhxr2QfMvNVjmDA0bv-xQgZQUYpLuyyaY');


// development
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');
date_default_timezone_set("America/New_York");

//Memcached
$mc = new Memcached();
$mc->addServer("localhost", 11211);
