<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require __DIR__ . '/vendor/autoload.php';

use SKAgarwal\GoogleApi\PlacesApi;
//header('Content-Type: application/json');
// move to .env later
//$googlePlaces = new PlacesApi('AIzaSyDhxr2QfMvNVjmDA0bv-xQgZQUYpLuyyaY');
$googlePlaces = new PlacesApi('AIzaSyCN5KN7kUwJ55lwj8mQcSrj3t5nE5nZ5l8');
$data = $googlePlaces->textSearch('electrician dallas texas');

$businesses = json_decode($data,true);
/*
foreach ($businesses['results'] as $business) {
	echo $business['name']. '<br>';
}
*/
?>

echo '<table><tbody><tr><th>Name</th><th>Address</th><th>Place ID</th><th>Rating</th><th>Latitude</th><th>Longitude</th></tr>';
		<?php foreach ($businesses['results'] as $business) : ?>
        <tr>
            <td> <?php echo $business['name']; ?> </td>
            <td> <?php echo $business['formatted_address']; ?> </td>
            <td> <?php echo $business['place_id']; ?> </td>
            <td> <?php echo $business['rating']; ?> </td>
            <td> <?php echo $business['geometry']['location']['lat']; ?> </td>
            <td> <?php echo $business['geometry']['location']['lng']; ?> </td>
        </tr>
		<?php endforeach; ?>
	</tbody>
</table>
